package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class GotoCommand extends Command {

    public GotoCommand( ) {
        super( "goto", "core.command.goto", false, "gc" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer( strings[0] );
            if ( target == null ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            if ( player.getServer().equals( target.getServer().getInfo() ) ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDu bist bereits auf diesen Server." );
                return;
            }
            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du bist zum Spieler " + target.getDisplayName() +
                    " §7gesprungen." );
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/goto §7[§fSpieler§7]" );
    }
}
