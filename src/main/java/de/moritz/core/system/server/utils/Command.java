package de.moritz.core.system.server.utils;

import com.google.common.collect.Sets;
import de.moritz.core.Main;
import de.moritz.core.system.player.GrafikPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Set;

/**
 * @author: toLowerCase
 */
public abstract class Command extends net.md_5.bungee.api.plugin.Command {

    private static Set<Command> commands = Sets.newHashSet();
    private String permission;
    private boolean console;

    public Command( String name ) {
        this( name, null, false );
    }

    public Command( String name, String permission, boolean console ) {
        this( name, permission, console, new String[0] );
    }

    public Command( String name, String permission, boolean console, String... aliases ) {
        super( name, null, aliases );
        this.permission = permission;
        this.console = console;
        ProxyServer.getInstance().getPluginManager().registerCommand( Main.getInstance(), this );
        Command.commands.add( this );
    }

    @Override
    public void execute( CommandSender commandSender, String[] strings ) {
        if ( !( commandSender instanceof ProxiedPlayer ) ) {
            if ( this.console ) {
                this.onConsole( commandSender, strings );
                return;
            }
            commandSender.sendMessage( "§cDieser Befehl ist nur für Spieler!" );
            return;
        }
        GrafikPlayer player = GrafikPlayer.getPlayer( (ProxiedPlayer) commandSender );
        if ( this.permission == null || player.hasPermission( permission ) ) {
            this.onPlayer( player, strings );
            return;
        }
        player.sendMessage( "§cSystem", "§cFür diesen Command hast du keine Rechte!" );
        if ( !player.hasPermission( "core.team.seeperm" ) )
            return;
        player.sendMessage( "§cSystem", "§cFehlende Permission§8: §e" + this.permission + "§c!" );
    }

    public abstract void onPlayer( GrafikPlayer player, String[] strings );

    public void onConsole( CommandSender commandSender, String[] strings ) {

    }

    public String getPerm( ) {
        return this.permission;
    }

    public static Set<Command> getCommands( ) {
        return Command.commands;
    }
}
