package de.moritz.core.system.server.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;

/**
 * @author: toLowerCase
 */
public class OnlineCommand extends Command {
    public OnlineCommand( ) {
        super( "online" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        int size = ProxyServer.getInstance().getOnlineCount();
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Aktuell " + ( size == 1 ? "ist" : "sind" ) + " §e" +
                size + " §7Spieler online." );
    }
}
