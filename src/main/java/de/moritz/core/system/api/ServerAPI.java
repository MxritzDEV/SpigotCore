package de.moritz.core.system.api;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author: toLowerCase
 */
public class ServerAPI {

    private String host;
    private int port;
    private Socket socket = new Socket();
    private String[] data = new String[999];

    public ServerAPI( String host, int port ) {
        this.host = host;
        this.port = port;

        try {
            socket.connect( new InetSocketAddress( host, port ) );
            OutputStream out = socket.getOutputStream();
            InputStream in = socket.getInputStream();
            out.write( 0xFE );

            int b;
            StringBuffer str = new StringBuffer();
            while ( ( b = in.read() ) != -1 ) {
                if ( b != 0 && b > 16 && b != 255 && b != 23 && b != 24 ) {
                    str.append( (char) b );
                }
            }

            data = str.toString().split( "§" );
            data[0] = data[0].substring( 1, data[0].length() );
        } catch ( Exception e ) {
        }
    }

    public String getMOTD( ) {
        return data[0];
    }

    public int getPlayerSize( ) {
        if ( data[1] != null )
            return Integer.parseInt( data[1] );
        return 0;
    }

    public int getMaxPlayers( ) {
        if ( data[2] != null )
            return Integer.parseInt( data[2] );
        return 0;
    }

    public void update( ) {
        try {
            socket.close();
            socket = new Socket( host, port );
            socket.connect( new InetSocketAddress( host, port ) );
            OutputStream out = socket.getOutputStream();
            InputStream in = socket.getInputStream();
            out.write( 0xFE );

            int b;
            StringBuffer str = new StringBuffer();
            while ( ( b = in.read() ) != -1 ) {
                if ( b != 0 && b > 16 && b != 255 && b != 23 && b != 24 ) {
                    str.append( (char) b );
                }
            }
            data = str.toString().split( "§" );
            data[0] = data[0].substring( 1, data[0].length() );
        } catch ( Exception e ) {
        }
    }

    public boolean isOnline( ) {
        return !isOffline();
    }

    public boolean isOffline( ) {
        return getMOTD() == null && getMaxPlayers() == 0 && getPlayerSize() == 0;
    }
}
