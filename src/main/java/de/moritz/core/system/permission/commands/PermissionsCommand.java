package de.moritz.core.system.permission.commands;

import de.moritz.core.system.permission.utils.GroupType;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermMethods;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import de.moritz.core.system.utils.UUIDFetcher;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermissionsCommand extends Command {

    public PermissionsCommand( ) {
        super( "permission", "core.command.permission", false, "perm" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 9 ) {
            if ( strings[0].equalsIgnoreCase( "group" ) ) {
                if ( strings[1].equalsIgnoreCase( "create" ) ) {
                    int id = 1;
                    try {
                        id = Integer.parseInt( strings[2] );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8: §c/perm group create " +
                                "§7[§fID§7] §7[§fName§7] §7[§fShortName§7] §7[§fColor§7] §7[§fUseFullName§7] " +
                                "§7[§fGroupType§7] §7[§fIsDefault§7]" );
                        return;
                    }
                    String name = strings[3];
                    String shortName = strings[4];
                    String color = strings[5];
                    boolean useFullName = strings[6].equalsIgnoreCase( "true" );
                    GroupType groupType = GroupType.USER;
                    try {
                        groupType = GroupType.valueOf( strings[7].toUpperCase() );
                    } catch ( IllegalArgumentException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cDieser GroupType ist nicht verfügbar!" );
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cVerfürbare GroupTypes:" );
                        for ( GroupType type : GroupType.values() ) {
                            player.sendMessage( "    §8- §7" + type.name() );
                        }
                        return;
                    }
                    boolean isDefault = strings[8].equalsIgnoreCase( "true" );
                    PermMethods.createGroup( player, id, name, shortName, color, useFullName, groupType, isDefault );
                    return;
                }
            }
            PermMethods.sendHelp( player );
            return;
        }
        if ( strings.length == 5 ) {
            if ( strings[0].equalsIgnoreCase( "user" ) ) {
                if ( strings[1].equalsIgnoreCase( "create" ) ) {
                    UUID uuid = UUIDFetcher.getUUID( strings[2] );
                    PermGroup permGroup = PermGroup.getDefault();
                    int days = -1;
                    try {
                        permGroup = PermGroup.getGroup( Integer.parseInt( strings[3] ) );
                        days = Integer.parseInt( strings[4] );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8:" +
                                " §c/perm user create §7[§fName§7] [§fID§7] [§fTage§7]" );
                    }
                    PermMethods.createUser( player, uuid, permGroup, days );
                    return;
                }
                if ( strings[1].equalsIgnoreCase( "edit" ) ) {
                    UUID uuid = UUIDFetcher.getUUID( strings[2] );
                    String what = strings[3];
                    String update = strings[4];
                    PermMethods.editUser( player, uuid, what, update );
                    return;
                }
            }
            PermMethods.sendHelp( player );
            return;
        }
        if ( strings.length == 4 ) {
            if ( strings[0].equalsIgnoreCase( "group" ) ) {
                if ( strings[2].equalsIgnoreCase( "add" ) ) {
                    try {
                        PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( strings[1] ) );
                        String permission = strings[3];
                        PermMethods.addPermission( player, permGroup, permission );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8:" +
                                " §c/perm group §7[§fID§7] §cadd §7[§fPermission§7]" );
                    }
                    return;
                }
                if ( strings[2].equalsIgnoreCase( "remove" ) ) {
                    try {
                        PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( strings[1] ) );
                        String permission = strings[3];
                        PermMethods.removePermission( player, permGroup, permission );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8:" +
                                " §c/perm group §7[§fID§7] §cremove §7[§fPermission§7]" );
                    }
                    return;
                }
                return;
            }
            PermMethods.sendHelp( player );
            return;
        }
        if ( strings.length == 3 ) {
            if ( strings[0].equalsIgnoreCase( "user" ) ) {
                if ( strings[1].equalsIgnoreCase( "info" ) ) {
                    UUID uuid = UUIDFetcher.getUUID( strings[2] );
                    PermMethods.infoUser( player, uuid );
                    return;
                }
                if ( strings[1].equalsIgnoreCase( "delete" ) ) {
                    UUID uuid = UUIDFetcher.getUUID( strings[2] );
                    PermMethods.removeUser( player, uuid );
                    return;
                }
            }
            if ( strings[0].equalsIgnoreCase( "group" ) ) {
                if ( strings[1].equalsIgnoreCase( "info" ) ) {
                    try {
                        PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( strings[2] ) );
                        PermMethods.infoGroup( player, permGroup );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8:" +
                                " §c/perm info group §7[§fID§7]" );
                    }
                    return;
                }
                if ( strings[1].equalsIgnoreCase( "delete" ) ) {
                    try {
                        PermGroup permGroup = PermGroup.getGroup( Integer.parseInt( strings[2] ) );
                        PermMethods.removeGroup( player, permGroup );
                    } catch ( NumberFormatException ex ) {
                        player.sendMessage( "§8┃ §6● §8┃ §6Permissions §8× §cNutze§8:" +
                                " §c/perm delete group §7[§fID§7]" );
                    }
                    return;
                }
            }
            PermMethods.sendHelp( player );
            return;
        }
        PermMethods.sendHelp( player );
    }
}
