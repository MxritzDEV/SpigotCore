package de.moritz.core.system.player;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public interface PlayerInfo {

    public UUID getUUID( );

    public String getName( );

    public String getIP( );

    public int getBans( );

    public int getMutes( );

    public int getKicks();

    public long getFirstJoin( );

    public long getLastLeave( );

    public long getOnlineTime( );

    public int getOnlineHours( );

}
