package de.moritz.core.system.server.commands;

import de.moritz.core.system.permission.utils.GroupType;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class TeamCommand extends Command {

    public TeamCommand( ) {
        super( "team" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        int count = 0;
        player.sendMessage( "§8┃ §b● §8┃ §bTeam §8× §7Aktuelle Teammitglieder auf §6GRAFIKBUG§7.§6de" );
        for ( Integer id : PermGroup.getGroups().keySet() ) {
            PermGroup permGroup = PermGroup.getGroup( id );
            if ( permGroup.getGroupType().equals( GroupType.USER ) )
                continue;
            player.sendMessage( "§8┃ §b● §8┃ §bTeam §8× " + permGroup.getColor() + permGroup.getName() );
            for ( UUID uuid : PermUser.getPermUsers().keySet() ) {
                PermUser permUser = PermUser.getUser( uuid );
                if ( permUser.getGroup().getId() != permGroup.getId() )
                    continue;
                player.sendMessage( "    §8- " + permGroup.getColor() + permUser.getName() );
                count++;
            }
        }
        player.sendMessage( "§8┃ §b● §8┃ §bTeam §8× §7Momentan besteht das Team aus §e" + count + " §7Mitgliedern." );
    }
}
