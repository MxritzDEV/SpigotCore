package de.moritz.core.system.player.listeners;

import de.moritz.core.system.player.GrafikOfflinePlayer;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Methods;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author: toLowerCase
 */
public class PlayerListener implements Listener {

    @EventHandler( priority = 100 )
    public void onPostLogin( PostLoginEvent event ) {
        ProxiedPlayer player = event.getPlayer();

        if ( GrafikOfflinePlayer.hasAccount( player.getUniqueId() ) ) {
            GrafikPlayer grafikPlayer = new GrafikPlayer( player );
            grafikPlayer.login();

            if ( !grafikPlayer.getPlayer().hasPermission( "core.team" ) )
                return;
            Methods.send( "§8┃ §b● §8┃ §bTeam §8× " + grafikPlayer.getDisplayName() + " §7ist jetzt §aonline§7.",
                    "core.team" );
            return;
        }

        GrafikOfflinePlayer offlinePlayer = new GrafikOfflinePlayer( player.getUniqueId() );
        offlinePlayer.create();

        GrafikPlayer grafikPlayer = GrafikPlayer.getPlayer( player );
        if ( !grafikPlayer.getPlayer().hasPermission( "core.team" ) )
            return;
        Methods.send( "§8┃ §b● §8┃ §bTeam §8× " + grafikPlayer.getDisplayName() + " §7ist jetzt §aonline§7.",
                "core.team" );
    }

    @EventHandler
    public void onSwitch( ServerSwitchEvent event ) {
        ProxiedPlayer player = event.getPlayer();

        TextComponent header = new TextComponent();
        header.setText( "\n§6GrafikbugDE §8× §7Minigames Netzwerk\n" +
                "§7Server §8➟ §e" + player.getServer().getInfo().getName() + "\n" );

        TextComponent footer = new TextComponent();
        footer.setText( "\n§7Webseite §8➟ §eGrafikbug.de  §8┃  §7TeamSpeak §8➟ §ets.Grafikbug.de\n" +
                "§7Hoster §8➟ §aEmeraldHost.de\n" +
                "" );

        player.setTabHeader( header, footer );
    }

    @EventHandler
    public void onDisconnect( PlayerDisconnectEvent event ) {
        GrafikPlayer grafikPlayer = GrafikPlayer.getPlayer( event.getPlayer() );
        if ( grafikPlayer.getPlayer().hasPermission( "core.team" ) ) {
            Methods.send( "§8┃ §b● §8┃ §bTeam §8× " + grafikPlayer.getDisplayName() + " §7ist jetzt §coffline§7.",
                    "core.team" );
        }
        grafikPlayer.logut();
    }
}
