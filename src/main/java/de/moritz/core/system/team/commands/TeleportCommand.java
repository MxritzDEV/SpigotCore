package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

/**
 * @author: toLowerCase
 */
public class TeleportCommand extends Command {

    public TeleportCommand( ) {
        super( "teleport", "core.command.teleport", false, "tp" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 2 ) {
            GrafikPlayer targetPlayer = GrafikPlayer.getPlayer( strings[0] );
            GrafikPlayer targetLocation = GrafikPlayer.getPlayer( strings[1] );
            if ( targetPlayer == null ) {
                player.sendMessage( "§cSystem", "§cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            if ( targetLocation == null ) {
                player.sendMessage( "§cSystem", "§cDer Spieler §e" + strings[1] +
                        " §cist aktuell nicht online!" );
                return;
            }
            targetPlayer.sendMessage( "§cSystem", "§7Du wurdest zu " + targetLocation.getDisplayName()
                    + " §7teleportiert." );
            targetLocation.sendMessage( "§cSystem", targetPlayer.getDisplayName() +
                    " §7wurde zu dir teleportiert." );
            targetPlayer.teleport( targetLocation );
            if ( targetLocation.equals( player ) )
                return;
            if ( targetLocation.equals( player ) )
                return;
            player.sendMessage( "§cSystem", "§7Du hast " + targetPlayer.getDisplayName() + " §7zu "
                    + targetLocation.getDisplayName() + " §7teleportiert." );
            return;
        }
        if ( strings.length == 1 ) {
            GrafikPlayer targetLocation = GrafikPlayer.getPlayer( strings[0] );
            if ( targetLocation == null ) {
                player.sendMessage( "§cSystem", "§cDer Spieler §e" + strings[1] +
                        " §cist aktuell nicht online!" );
                return;
            }
            player.sendMessage( "§cSystem", "§7Du wurdest zu " + targetLocation.getDisplayName()
                    + " §7teleportiert." );
            targetLocation.sendMessage( "§cSystem", player.getDisplayName() +
                    " §7wurde zu dir teleportiert." );
            player.teleport( targetLocation );
            return;
        }
        player.sendMessage( "§cSystem", "§cNutze§8: §c/teleport §7[§fSpieler§7] §7<§fSpieler§7>" );
    }
}
