package de.moritz.core.system.permission.listeners;

import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermMethods;
import de.moritz.core.system.permission.utils.PermUser;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author: toLowerCase
 */
public class PermissionListener implements Listener {

    @EventHandler
    public void onPostLogin( PostLoginEvent event ) {
        ProxiedPlayer player = event.getPlayer();
        if ( PermUser.hasAccount( player.getUniqueId() ) ) {
            PermUser permUser = PermUser.getUser( player.getUniqueId() );
            permUser.setName( player.getName() );
            if ( permUser.getExpiration() != -1 && permUser.getExpiration() < System.currentTimeMillis() ) {
                permUser.setGroup( PermGroup.getDefault() );
                permUser.setExpiration( -1 );
                PermMethods.reload();
            }

            player.setDisplayName( permUser.getGroup().getColor() + player.getName() );
            return;
        }
        PermUser permUser = new PermUser( player.getUniqueId() );
        permUser.setName( player.getName() );
        permUser.setExpiration( -1 );
        permUser.setGroup( PermGroup.getDefault() );
        permUser.create();

        PermMethods.reload();
        player.setDisplayName( permUser.getGroup().getColor() + player.getName() );
    }

    @EventHandler
    public void onPermissionCheck( PermissionCheckEvent event ) {
        if ( !( event.getSender() instanceof ProxiedPlayer ) )
            return;
        PermUser player = PermUser.getUser( ( (ProxiedPlayer) event.getSender() ).getUniqueId() );

        if ( player.hasPermission( event.getPermission() ) || player.hasPermission( "*" ) ) {
            event.setHasPermission( true );
            return;
        }
        event.setHasPermission( false );
    }
}
