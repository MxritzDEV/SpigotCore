package de.moritz.core.system.permission.utils;

import com.google.common.collect.Maps;
import de.moritz.core.system.mysql.MySQLUtils;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author: toLowerCase
 */
public class PermUser {

    private static HashMap<UUID, PermUser> permUsers = Maps.newHashMap();
    private UUID uuid;
    private String name;
    private PermGroup group;
    private long expiration;

    public PermUser( UUID uuid ) {
        this.uuid = uuid;
    }

    public boolean hasAccount( ) {
        if ( PermUser.permUsers.containsKey( uuid ) )
            return true;
        return MySQLUtils.getPermissionMySQL().existsUser( this.uuid );
    }

    public void create( ) {
        if ( hasAccount() )
            return;

        PermUser.permUsers.put( this.uuid, this );
        MySQLUtils.getPermissionMySQL().addUser( this.uuid, this.group == null ? PermGroup.getDefault() :
                this.group, this.expiration );
    }

    public void remove( ) {
        if ( !hasAccount() )
            return;
        PermUser.permUsers.remove( this.uuid );
        MySQLUtils.getPermissionMySQL().removeUser( this.uuid );
    }

    public void save( ) {
        if ( !hasAccount() )
            return;
        MySQLUtils.getPermissionMySQL().updateUser( this );
    }

    public UUID getUUID( ) {
        return this.uuid;
    }

    public String getName( ) {
        return this.name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public PermGroup getGroup( ) {
        return this.group;
    }

    public void setGroup( PermGroup group ) {
        this.group = group;
    }

    public void setExpiration( int days ) {
        this.expiration = days == -1 ? -1 : this.calculateExpiration( days );
    }

    public void setExpiration( long expiration ) {
        this.expiration = expiration;
    }

    public long getExpiration( ) {
        return this.expiration;
    }

    private long calculateExpiration( int days ) {
        if ( this.expiration == -1 ) {
            return -1;
        }
        return System.currentTimeMillis() + TimeUnit.DAYS.toMillis( days );
    }

    public boolean hasPermission( String permission ) {
        return this.getGroup().hasPermission( permission );
    }

    public static HashMap<UUID, PermUser> getPermUsers( ) {
        return PermUser.permUsers;
    }

    public static void setPermUsers( HashMap<UUID, PermUser> permUsers ) {
        PermUser.permUsers = permUsers;
    }

    public static PermUser getUser( UUID uuid ) {
        return PermUser.permUsers.get( uuid );
    }

    public static boolean hasAccount( UUID uuid ) {
        return PermUser.permUsers.containsKey( uuid );
    }
}
