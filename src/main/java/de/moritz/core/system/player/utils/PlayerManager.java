package de.moritz.core.system.player.utils;

import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.player.GrafikOfflinePlayer;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PlayerManager {

    public static void init( ) {
        GrafikOfflinePlayer.setOfflinePlayers( MySQLUtils.getPlayerMySQL().getOfflinePlayers() );
    }

    public static void save( ) {
        for ( UUID uuid : GrafikOfflinePlayer.getOfflinePlayers().keySet() ) {
            GrafikOfflinePlayer.getOfflinePlayer( uuid ).save();
        }
    }
}
