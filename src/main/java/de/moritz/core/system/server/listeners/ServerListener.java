package de.moritz.core.system.server.listeners;

import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class ServerListener implements Listener {

    @EventHandler
    public void onPing( ProxyPingEvent event ) {
        ServerPing serverPing = event.getResponse();

        ServerPing.Players players = serverPing.getPlayers();
        ServerPing.PlayerInfo[] playerInfos = new ServerPing.PlayerInfo[]{
                new ServerPing.PlayerInfo( "§8§m--------------------------------", UUID.randomUUID() ),
                new ServerPing.PlayerInfo( "§8× §7Unser Fortschritt", UUID.randomUUID() ),
                new ServerPing.PlayerInfo( "§8➟ §8[§e||||||§7|||||||||||||||||||||||||||||||||||||||" +
                        "|||||||||||||||§8] §a12%", UUID.randomUUID() ),
                new ServerPing.PlayerInfo( "§8§m--------------------------------", UUID.randomUUID() ),
        };

        players.setSample( playerInfos );
        serverPing.setPlayers( players );

        String line1 = "§8§l┃ §6● §8┃ §6GrafikbugDE §8× §7Minigames Netzwerk §8➟ §e1.8§7-§e1.11.2";
        String line2 = "§8§l┃ §c● §8┃ §cINFO §8➟ §7Wir arbeiten gerade an der §e2.0§7!";

        serverPing.setDescription( line1 + "\n" + line2 );

        event.setResponse( serverPing );
    }
}
