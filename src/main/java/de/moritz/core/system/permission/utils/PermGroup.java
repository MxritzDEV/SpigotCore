package de.moritz.core.system.permission.utils;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.system.mysql.MySQLUtils;
import net.md_5.bungee.api.ProxyServer;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermGroup {

    private static HashMap<Integer, PermGroup> groups = Maps.newHashMap();
    private int id;
    private String name;
    private String shortName;
    private String color;
    private boolean useFullName;
    private GroupType groupType;
    private Set<String> permissions;
    private boolean defaultGroup;

    public PermGroup( int id ) {
        this.id = id;
    }

    public void create( ) {
        this.permissions = Sets.newHashSet();
        PermGroup.groups.put( this.id, this );
        MySQLUtils.getPermissionMySQL().addGroup( this );
    }

    public void delete( ) {
        removeAllPermissions();
        removeAllUsers();
        PermGroup.groups.remove( this.id );
        MySQLUtils.getPermissionMySQL().removeGroup( this );
    }

    public void save( ) {
        MySQLUtils.getPermissionMySQL().updateGroup( this );
    }

    private void removeAllPermissions( ) {
        MySQLUtils.getPermissionMySQL().clearPermission( this );
        this.permissions.clear();
    }

    private void removeAllUsers( ) {
        for ( UUID uuid : PermUser.getPermUsers().keySet() ) {
            PermUser permUser = PermUser.getUser( uuid );
            if ( permUser.getGroup() != this )
                continue;
            if ( ProxyServer.getInstance().getPlayer( uuid ) != null ) {
                ProxyServer.getInstance().getPlayer( uuid ).disconnect(
                        "§8┃ §6● §8┃ §6GRAFIKBUG§7.§6de §8× §aRangänderung\n\n" +
                                "§7Du hast ein neuen §eRang §7erhalten.\n" +
                                "§7Neuer Rang §8➟ " + PermGroup.getDefault().getColor() + PermGroup.getDefault().getName() );
            }
            permUser.remove();
        }
    }

    public void addPermission( String permission ) {
        this.permissions.add( permission );
    }

    public void removePermission( String permission ) {
        if ( !this.permissions.contains( permission ) )
            return;
        this.permissions.remove( permission );
    }

    public static HashMap<Integer, PermGroup> getGroups( ) {
        return PermGroup.groups;
    }

    public static void setGroups( HashMap<Integer, PermGroup> groups ) {
        PermGroup.groups = groups;
    }

    public String getName( ) {
        return this.name;
    }

    public void setName( String name ) {
        name = name.replace( "%ae%", "ä" );
        this.name = name;
    }

    public int getId( ) {
        return this.id;
    }

    public void setId( int id ) {
        this.id = id;
    }

    public String getShortName( ) {
        return this.shortName;
    }

    public void setShortName( String shortName ) {
        this.shortName = shortName;
    }

    public String getColor( ) {
        return this.color.replace( "&", "§" );
    }

    public void setColor( String color ) {
        this.color = color;
    }

    public boolean isUseFullName( ) {
        return this.useFullName;
    }

    public void setUseFullName( boolean useFullName ) {
        this.useFullName = useFullName;
    }

    public GroupType getGroupType( ) {
        return this.groupType;
    }

    public void setGroupType( GroupType groupType ) {
        this.groupType = groupType;
    }

    public Set<String> getPermissions( ) {
        return this.permissions;
    }

    public void setPermissions( Set<String> permissions ) {
        this.permissions = permissions;
    }

    public boolean isDefaultGroup( ) {
        return defaultGroup;
    }

    public void setDefaultGroup( boolean defaultGroup ) {
        this.defaultGroup = defaultGroup;
    }

    public static PermGroup getDefault( ) {
        PermGroup permGroup = null;
        for ( Integer id : PermGroup.groups.keySet() ) {
            if ( !PermGroup.getGroup( id ).isDefaultGroup() )
                continue;
            permGroup = PermGroup.getGroup( id );
        }
        return permGroup;
    }

    public static PermGroup getGroup( int id ) {
        PermGroup permGroup;
        try {
            permGroup = PermGroup.groups.get( id );
        } catch ( IllegalArgumentException ex ) {
            return null;
        }
        return permGroup;
    }

    public boolean hasPermission( String permission ) {
        return this.permissions.contains( permission );
    }
}