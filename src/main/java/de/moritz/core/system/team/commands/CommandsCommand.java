package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * @author: toLowerCase
 */
public class CommandsCommand extends Command {

    public CommandsCommand( ) {
        super( "commands", "core.command.commands", false, "cmds" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Hier eine Liste aller verfügbaren Commands." );
        for ( Command command : Command.getCommands() ) {
            player.sendMessage( "§8§m--------------------------------------------" );
            TextComponent component = new TextComponent( "§e/" + command.getName() );
            component.setClickEvent( new ClickEvent( ClickEvent.Action.SUGGEST_COMMAND, "/" + command.getName() ) );

            player.sendMessage( new TextComponent( "§8┃ §c● §8┃ §cSystem §8× §7Command §8➟ " ), component );
            if ( command.getPerm() != null )
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Permission §8➟ §e" + command.getPerm() );
        }
        player.sendMessage( "§8§m--------------------------------------------" );
    }
}