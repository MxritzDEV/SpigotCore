package de.moritz.core.system.player.mysql;

import com.google.common.collect.Maps;
import de.moritz.core.system.mysql.Database;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.player.GrafikOfflinePlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: toLowerCase
 */
public class PlayerMySQL implements Database<Connection> {

    private Connection connection;
    private ExecutorService pool;

    @Override
    public void connect( ) {
        this.connection = MySQLUtils.getConnection();
        this.pool = Executors.newCachedThreadPool();
        createTable();
    }

    @Override
    public void disconnect( ) {
        try {
            this.connection.close();
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        }
        this.pool.shutdown();
    }

    @Override
    public void createTable( ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS players__user" +
                        " (UUID VARCHAR (100), NAME VARCHAR (100), IP VARCHAR (100), BANS VARCHAR (100)," +
                        " MUTES VARCHAR (100), KICKS VARCHAR (100), FIRSTJOIN VARCHAR (100), LASTLEAVE VARCHAR (100), " +
                        "ONLINETIME VARCHAR (100))" );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public boolean hasAccount( UUID uuid ) {
        boolean exists = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.connection.prepareStatement( "SELECT * FROM players__user WHERE UUID = ?" );
            preparedStatement.setString( 1, uuid.toString() );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                exists = true;
                System.out.println( true );
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        return exists;
    }

    public void insertPlayer( ProxiedPlayer player ) {
        System.out.println( hasAccount( player.getUniqueId() ) );
        if ( hasAccount( player.getUniqueId() ) )
            return;
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "INSERT INTO players__user (UUID, NAME, IP, BANS, " +
                        "MUTES, KICKS, FIRSTJOIN, LASTLEAVE, ONLINETIME) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" );
                preparedStatement.setString( 1, player.getUniqueId().toString() );
                preparedStatement.setString( 2, player.getName() );
                preparedStatement.setString( 3, player.getAddress().getAddress().toString()
                        .replace( "/", "" ) );
                preparedStatement.setInt( 4, 0 );
                preparedStatement.setInt( 5, 0 );
                preparedStatement.setInt( 6, 0 );
                preparedStatement.setLong( 7, System.currentTimeMillis() );
                preparedStatement.setString( 8, "null" );
                preparedStatement.setLong( 9, 0 );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public void updatePlayer( GrafikOfflinePlayer offlinePlayer ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "UPDATE players__user SET NAME = ?, IP = ?," +
                        " BANS = ?, MUTES = ?, KICKS = ?, LASTLEAVE = ?, ONLINETIME = ? WHERE UUID = ?" );

                preparedStatement.setString( 1, offlinePlayer.getName() );
                preparedStatement.setString( 2, offlinePlayer.getIP() );
                preparedStatement.setInt( 3, offlinePlayer.getBans() );
                preparedStatement.setInt( 4, offlinePlayer.getMutes() );
                preparedStatement.setInt( 5, offlinePlayer.getKicks() );
                preparedStatement.setLong( 6, offlinePlayer.getLastLeave() );
                preparedStatement.setLong( 7, offlinePlayer.getOnlineTime() );
                preparedStatement.setString( 8, offlinePlayer.getUUID().toString() );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public HashMap<UUID, GrafikOfflinePlayer> getOfflinePlayers( ) {
        HashMap<UUID, GrafikOfflinePlayer> offlinePlayers = Maps.newHashMap();
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT * FROM players__user" );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    UUID uuid = UUID.fromString( resultSet.getString( "UUID" ) );
                    GrafikOfflinePlayer offlinePlayer = new GrafikOfflinePlayer( uuid );
                    offlinePlayer.setName( resultSet.getString( "NAME" ) );
                    offlinePlayer.setIp( resultSet.getString( "IP" ) );
                    offlinePlayer.setBans( resultSet.getInt( "BANS" ) );
                    offlinePlayer.setMutes( resultSet.getInt( "MUTES" ) );
                    offlinePlayer.setKicks( resultSet.getInt( "KICKS" ) );
                    offlinePlayer.setFirstJoin( resultSet.getLong( "FIRSTJOIN" ) );
                    offlinePlayer.setLastLeave( resultSet.getLong( "LASTLEAVE" ) );
                    offlinePlayer.setOnlineTime( resultSet.getLong( "ONLINETIME" ) );
                    offlinePlayer.load();
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return offlinePlayers;
    }

    @Override
    public boolean isConnected( ) {
        return this.connection != null;
    }

    @Override
    public Connection getConnection( ) {
        return this.connection;
    }
}
