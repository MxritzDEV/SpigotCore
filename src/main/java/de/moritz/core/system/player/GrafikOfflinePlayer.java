package de.moritz.core.system.player;

import com.google.common.collect.Maps;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.permission.utils.PermUser;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class GrafikOfflinePlayer implements PlayerInfo {

    private static HashMap<UUID, GrafikOfflinePlayer> offlinePlayers = Maps.newHashMap();
    private UUID uuid;
    private String name;
    private String ip;
    private int bans;
    private int mutes;
    private int kicks;
    private long firstJoin;
    private long lastLeave;
    private long onlineTime;

    public GrafikOfflinePlayer( UUID uuid ) {
        this.uuid = uuid;
    }

    public void create( ) {
        if ( GrafikOfflinePlayer.hasAccount( this.uuid ) )
            return;
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer( this.uuid );
        setName( player.getName() );
        setIp( player.getAddress().getAddress().toString().replace( "/", "" ) );
        setBans( 0 );
        setMutes( 0 );
        setFirstJoin( System.currentTimeMillis() );
        setOnlineTime( 0 );
        GrafikOfflinePlayer.offlinePlayers.put( this.uuid, this );

        GrafikPlayer grafikPlayer = new GrafikPlayer( player );
        grafikPlayer.login();

        MySQLUtils.getPlayerMySQL().insertPlayer( player );

        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
            if ( !serverInfo.getName().toLowerCase().contains( "lobby" ) )
                continue;
            try {
                byteOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream( byteOutputStream );
                dataOutputStream.writeUTF( "createPlayer" );
                dataOutputStream.writeUTF( this.uuid.toString() );
                serverInfo.sendData( "Player", byteOutputStream.toByteArray() );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                try {
                    dataOutputStream.close();
                    byteOutputStream.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void load( ) {
        GrafikOfflinePlayer.offlinePlayers.put( this.uuid, this );
    }

    public void save( ) {
        MySQLUtils.getPlayerMySQL().updatePlayer( this );
    }

    public void setName( String name ) {
        this.name = name;
    }

    public void setIp( String ip ) {
        this.ip = ip;
    }

    public void setBans( int bans ) {
        this.bans = bans;
    }

    public void setMutes( int mutes ) {
        this.mutes = mutes;
    }

    public void setKicks( int kicks ) {
        this.kicks = kicks;
    }

    public void setFirstJoin( long firstJoin ) {
        this.firstJoin = firstJoin;
    }

    public void setLastLeave( long lastLeave ) {
        this.lastLeave = lastLeave;
    }

    public void setOnlineTime( long onlineTime ) {
        this.onlineTime = onlineTime;
    }

    @Override
    public UUID getUUID( ) {
        return this.uuid;
    }

    @Override
    public String getName( ) {
        return this.name;
    }

    @Override
    public String getIP( ) {
        return this.ip;
    }

    @Override
    public int getBans( ) {
        return this.bans;
    }

    @Override
    public int getMutes( ) {
        return this.mutes;
    }

    @Override
    public int getKicks( ) {
        return this.kicks;
    }

    @Override
    public long getFirstJoin( ) {
        return this.firstJoin;
    }

    @Override
    public long getLastLeave( ) {
        return this.lastLeave;
    }

    @Override
    public long getOnlineTime( ) {
        return this.onlineTime;
    }

    @Override
    public int getOnlineHours( ) {
        return this.bans;
    }

    public boolean hasPermission( String permission ) {
        return PermUser.getUser( this.uuid ).hasPermission( "*" ) ||
                PermUser.getUser( this.uuid ).hasPermission( permission );
    }

    public static boolean hasAccount( UUID uuid ) {
        return GrafikOfflinePlayer.getOfflinePlayers().containsKey( uuid );
    }

    public static GrafikOfflinePlayer getOfflinePlayer( UUID uuid ) {
        return GrafikOfflinePlayer.offlinePlayers.get( uuid );
    }

    public static void setOfflinePlayers( HashMap<UUID, GrafikOfflinePlayer> offlinePlayers ) {
        GrafikOfflinePlayer.offlinePlayers = offlinePlayers;
    }

    public static HashMap<UUID, GrafikOfflinePlayer> getOfflinePlayers( ) {
        return GrafikOfflinePlayer.offlinePlayers;
    }
}
