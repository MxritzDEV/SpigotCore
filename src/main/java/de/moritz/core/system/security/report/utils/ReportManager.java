package de.moritz.core.system.security.report.utils;

import de.moritz.core.Main;
import de.moritz.core.system.server.utils.Methods;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.concurrent.TimeUnit;

/**
 * @author: toLowerCase
 */
public class ReportManager implements Runnable {

    private ScheduledTask schdule;

    public ReportManager( ) {
        this.schdule = ProxyServer.getInstance().getScheduler().schedule( Main.getInstance(), this, 0, 1, TimeUnit.MINUTES );
    }

    public void stop( ) {
        this.schdule.cancel();
    }

    @Override
    public void run( ) {
        int reports = Report.getReports().size();
        if ( reports == 0 )
            return;
        Methods.send( "§cReport", "§7Es sind noch §e" + reports + ( reports == 1 ? " §cReport offen." :
                " §cReports offen." ), "core.report.see" );
    }
}
