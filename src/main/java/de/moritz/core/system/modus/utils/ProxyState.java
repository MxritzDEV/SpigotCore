package de.moritz.core.system.modus.utils;

/**
 * @author: toLowerCase
 */
public enum ProxyState {

    LOADING( "§8» §cLoading", "core.join.loading", "§8┃ §6● §8┃ §6GrafikbugDE §8× §aNetzwerkstart\n\n" +
            "§7Das §eNetzwerk §7startet gerade.\n§7Versuche es gleich noch einmal." ),
    STOPPING( "§8» §cStopping", "core.join.stopping", "§8┃ §6● §8┃ §6GrafikbugDE §8× §aNetzwerkneustart\n\n" +
            "§7Du wurdest gekickt.\n§7Das §eNetzwerk §7startet gleich wieder." ),
    WARTUNG( "§8» §cWartungen", "core.join.wartung", "§8┃ §6● §8┃ §6GrafikbugDE §8× §aWartungsarbeiten\n\n" +
            "§7Das §eNetzwerk §7ist momentan in §cWartungsarbeiten.\n§7Schau doch mal auf §bTwitter §f@grafikbug §7vorbei." ),
    CLOSEDBETA( "§8» §cClosedBeta", "core.join.closedbeta", "§8┃ §6● §8┃ §6GrafikbugDE §8× §aClosedBeta\n\n" +
            "§7Das §eNetzwerk §7ist momentan in einer §cClosedBeta.\n§7Nur §6Premium-Spieler §7können joinen.\n§7Shop " +
            "§8➟ §eshop.grafikbug.de" ),
    OPENBETA( "§8» §cOpenBeta", null, null ),
    FULL( null, "core.join.full", "§8┃ §6● §8┃ §6GrafikbugDE §8× §aVoll\n\n" +
            "§7Das §eNetzwerk §7ist momentan in §cvoll.\n§7Um trotzdem joinen zu können, benötigst du §6Premium.\n§7Shop " +
            "§8➟ §eshop.grafikbug.de" ),
    ONLINE( null, null, null );

    private String displayName;
    private String permission;
    private String kickreason;

    private ProxyState( String displayName, String permission, String kickreason ) {
        this.displayName = displayName;
        this.permission = permission;
        this.kickreason = kickreason;
    }

    public String getDisplayName( ) {
        return this.displayName;
    }

    public String getPermission( ) {
        return this.permission;
    }

    public String getKickreason( ) {
        return this.kickreason;
    }
}
