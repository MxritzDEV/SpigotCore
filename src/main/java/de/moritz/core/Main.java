package de.moritz.core;

import de.moritz.core.system.modus.commands.ModusCommand;
import de.moritz.core.system.modus.listeners.ModusListener;
import de.moritz.core.system.modus.utils.ProxyState;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.permission.commands.PermissionsCommand;
import de.moritz.core.system.permission.listeners.PermissionListener;
import de.moritz.core.system.permission.utils.PermManager;
import de.moritz.core.system.player.listeners.PlayerListener;
import de.moritz.core.system.player.utils.PlayerManager;
import de.moritz.core.system.security.kick.commands.KickCommand;
import de.moritz.core.system.security.report.utils.ReportManager;
import de.moritz.core.system.server.commands.OnlineCommand;
import de.moritz.core.system.server.commands.PingCommand;
import de.moritz.core.system.server.commands.TeamCommand;
import de.moritz.core.system.server.commands.WhereamiCommand;
import de.moritz.core.system.server.listeners.ServerListener;
import de.moritz.core.system.server.utils.Methods;
import de.moritz.core.system.team.commands.*;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.concurrent.TimeUnit;

/**
 * @author: toLowerCase
 */
public class Main extends Plugin {

    private static Main instance;
    private static ProxyState proxyState;
    private static ReportManager reportManager;

    public static Main getInstance( ) {
        return instance;
    }

    public static ProxyState getProxyState( ) {
        return proxyState;
    }

    public static void setProxyState( ProxyState proxyState ) {
        Main.proxyState = proxyState;
    }

    public static ReportManager getReportManager( ) {
        return Main.reportManager;
    }

    @Override
    public void onLoad( ) {
        Main.instance = this;
    }

    @Override
    public void onEnable( ) {
        Main.proxyState = ProxyState.LOADING;
        Main.reportManager = new ReportManager();
        MySQLUtils.load();
        PermManager.init();
        PlayerManager.init();
        loadCommands();
        loadListeners();

        ProxyServer.getInstance().registerChannel( "Permission" );
        ProxyServer.getInstance().registerChannel( "Player" );

        ProxyServer.getInstance().getScheduler().schedule( this, new Runnable() {
            @Override
            public void run( ) {
                if ( Main.proxyState != ProxyState.LOADING )
                    return;
                Main.proxyState = ProxyState.WARTUNG;
                Methods.send( "§cSystem", "§7Der Modus wurde vom §bSystem §7auf §e" +
                        Main.proxyState.name() + " §7geändert", "core.command.modus" );
            }
        }, 5, TimeUnit.MINUTES );
    }

    @Override
    public void onDisable( ) {
        MySQLUtils.disconnect();
    }

    private void loadCommands( ) {
        new PingCommand();
        new FindCommand();
        new WhereamiCommand();
        new GotoCommand();
        new EndCommand();
        new BroadcastCommand();
        new OnlineCommand();
        new TeamChatCommand();
        new ModusCommand();
        new GlistCommand();
        new CommandsCommand();
        new SendCommand();
        new PermissionsCommand();
        new TeamCommand();
        new ServerCommand();
        new BPluginsCommand();
        new ClearchatCommand();
        new TeleportCommand();
        new NotifyCommand();
        new KickCommand();
    }

    public void loadListeners( ) {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener( this, new ModusListener() );
        pluginManager.registerListener( this, new PermissionListener() );
        pluginManager.registerListener( this, new ServerListener() );
        pluginManager.registerListener( this, new PlayerListener() );
    }
}
