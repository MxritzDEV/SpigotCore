package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * @author: toLowerCase
 */
public class BPluginsCommand extends Command {

    public BPluginsCommand( ) {
        super( "bplugins", "core.command.plugins", false, "bpl" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Hier eine Liste aller §ePlugins §7die auf diesem Server sind§8:" );
        for ( Plugin plugin : ProxyServer.getInstance().getPluginManager().getPlugins() ) {
            player.sendMessage( "    §8- §e" + plugin.getDescription().getName()
                    + " §7[" + plugin.getDescription().getVersion() + "]" );
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der Server hat ingesammt §e" +
                ProxyServer.getInstance().getPluginManager().getPlugins().size() + " §7Plugins." );
    }
}
