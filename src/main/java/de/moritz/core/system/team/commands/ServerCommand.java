package de.moritz.core.system.team.commands;

import de.moritz.core.system.api.ServerAPI;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;

/**
 * @author: toLowerCase
 */
public class ServerCommand extends Command {

    public ServerCommand( ) {
        super( "server", "core.command.server", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( strings[0] );
            if ( serverInfo == null ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDieser Server ist nicht verfügbar. " +
                        "Benutze /server um alle verfügbaren Server anzuzeigen." );
                return;
            }
            ServerAPI serverAPI = new ServerAPI( "localhost", serverInfo.getAddress().getPort() );
            if ( serverAPI.isOffline() ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Server §e" + serverInfo.getName()
                        + " §cist zurzeit Offline." );
                return;
            }
            if ( serverInfo.equals( player.getServer() ) ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDu bist bereits auf diesem Server." );
                return;
            }
            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du wirst mit §e" + serverInfo.getName() + " §7verbunden." );
            player.teleport( serverInfo );
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Dein aktueller Server auf dem du verbunden bist§8: §e" +
                player.getServer().getName() );
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Hier siehst du alle verfügbaren Server§8:" );
        for ( String name : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( name );
            ServerAPI serverAPI = new ServerAPI( "localhost", serverInfo.getAddress().getPort() );
            serverAPI.update();

            TextComponent online = new TextComponent( "§7[§aJoin§7]" );
            online.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND,
                    "/server " + serverInfo.getName() ) );

            player.sendMessage( new TextComponent( "    §8- §e" + serverInfo.getName() +
                            " §7[" + serverInfo.getPlayers().size() + "] §8➟ " ),
                    ( serverAPI.isOnline() ? online : new TextComponent( "§7[§cOffline§7]" ) ) );
        }
    }
}
