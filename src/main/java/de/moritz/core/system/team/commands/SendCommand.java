package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class SendCommand extends Command {

    public SendCommand( ) {
        super( "send", "core.command.send", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 2 ) {
            try {
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( strings[1] );
                if ( strings[0].equalsIgnoreCase( "all" ) ) {
                    for ( ProxiedPlayer all : ProxyServer.getInstance().getPlayers() ) {
                        if ( all.getUniqueId().toString().equalsIgnoreCase( player.getUUID().toString() ) )
                            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du hast alle Spieler auf den Server §e"
                                    + serverInfo.getName() + " §7teleportiert." );
                        else
                            all.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du wurdest von " + player.getDisplayName() +
                                    " §7auf dem Server §e" + serverInfo.getName() + " §7teleportiert." );
                        send( all, serverInfo );
                    }
                    return;
                }
                if ( strings[0].equalsIgnoreCase( "server" ) ) {
                    for ( ProxiedPlayer all : player.getServer().getPlayers() ) {
                        if ( all.getUniqueId().toString().equalsIgnoreCase( player.getUUID().toString() ) )
                            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du hast alle Spieler, von deinem Server, " +
                                    "auf den Server §e" + serverInfo.getName() + " §7teleportiert." );
                        else
                            all.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du wurdest von " + player.getDisplayName()
                                    + " §7auf dem Server §e" + serverInfo.getName() + " §7teleportiert." );
                        send( all, serverInfo );
                    }
                    return;
                }
                ProxiedPlayer target = ProxyServer.getInstance().getPlayer( strings[0] );
                if ( target == null ) {
                    player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Spieler §e" + strings[0] +
                            " §cist aktuell nicht online!" );
                    return;
                }
                send( target, serverInfo );
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du hast " + target.getDisplayName() +
                        " §7auf den Server §e" + serverInfo.getName() + " §7teleportiert." );
                target.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du wurdest von " + player.getDisplayName() +
                        " §7auf dem Server §e" + serverInfo.getName() + " §7teleportiert." );
                return;
            } catch ( NullPointerException ex ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDieser Server ist nicht verfügbar." );
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Hier eine Liste aller verfügbaren Server." );
                for ( String key : ProxyServer.getInstance().getServers().keySet() ) {
                    player.sendMessage( "    §8- §e" + ProxyServer.getInstance().getServerInfo( key ).getName() );
                }
                return;
            }
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/send §7[§fServer/Spieler/all§7] §7[§fServer§7]" );
    }

    private void send( ProxiedPlayer player, ServerInfo serverInfo ) {
        if ( player.getServer().getInfo().equals( serverInfo ) )
            return;
        player.connect( serverInfo );
    }
}
