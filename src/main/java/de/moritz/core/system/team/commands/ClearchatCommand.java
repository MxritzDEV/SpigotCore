package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class ClearchatCommand extends Command {

    public ClearchatCommand( ) {
        super( "clearchat", "core.command.clearchat", false, "cc" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        for ( ProxiedPlayer all : player.getServer().getPlayers() ) {
            if ( all.hasPermission( "core.command.clearchat" ) ) {
                for ( int i = 0; i < 10; i++ ) {
                    all.sendMessage( "" );
                }
                all.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der Chat wurde von " + player.getDisplayName() +
                        " §7geleert." );
                continue;
            }
            for ( int i = 0; i < 250; i++ ) {
                all.sendMessage( "" );
            }
            all.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der Chat wurde von " + player.getDisplayName() +
                    " §7geleert." );
        }
    }
}
