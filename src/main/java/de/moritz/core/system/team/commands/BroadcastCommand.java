package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class BroadcastCommand extends Command {
    public BroadcastCommand( ) {
        super( "broadcast", "core.command.broadcast", false, "bc", "alert" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 0 ) {
            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/broadcast §7[§fNachricht...§7]" );
            return;
        }
        StringBuilder builder = new StringBuilder();
        for ( String string : strings ) {
            builder.append( string ).append( " " );
        }
        for ( ProxiedPlayer all : ProxyServer.getInstance().getPlayers() ) {
            all.sendMessage( "" );
            all.sendMessage( "§8┃ §c● §8┃ §cRundruf §8× §7" + builder.toString().replace( "&", "§" ) );
            all.sendMessage( "" );
        }
    }
}
