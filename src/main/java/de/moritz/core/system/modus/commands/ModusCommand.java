package de.moritz.core.system.modus.commands;

import de.moritz.core.Main;
import de.moritz.core.system.modus.utils.ProxyState;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import de.moritz.core.system.server.utils.Methods;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class ModusCommand extends Command {

    private static boolean wartung = false;


    public ModusCommand( ) {
        super( "modus", "core.command.modus", false, "setmodus" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            try {
                ProxyState proxyState = ProxyState.valueOf( strings[0].toUpperCase() );
                if ( Main.getProxyState() == proxyState ) {
                    player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDieser Modus ist schon gesetzt!" );
                    return;
                }
                if ( proxyState == ProxyState.LOADING || proxyState == ProxyState.FULL ) {
                    player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDiesen Modus kannst du nicht setzten!" );
                    return;
                }

                Methods.send( "§cSystem", "§7Der Modus wurde von " + player.getDisplayName() +
                        " §7auf §e" + proxyState.name() + " §7geändert", "core.command.modus" );

                Main.setProxyState( proxyState );
                if ( proxyState.getKickreason() == null )
                    return;

                for ( ProxiedPlayer all : ProxyServer.getInstance().getPlayers() ) {
                    if ( all.hasPermission( proxyState.getPermission() ) )
                        continue;
                    all.disconnect( proxyState.getKickreason() );
                }
            } catch ( IllegalArgumentException ex ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDieser Modus ist nicht verfügbar!" );
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cVerfürbare Modi:" );
                for ( ProxyState state : ProxyState.values() ) {
                    player.sendMessage( "    §8- §e" + state );
                }
                return;
            }
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Aktuell ist der Modus §e" + Main.getProxyState().name()
                + "§7." );
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/modus §7[§fModus§7] §cum einen neuen Modus zu " +
                "setzen." );
    }
}
