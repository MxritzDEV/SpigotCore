package de.moritz.core.system.mysql;

import de.moritz.core.system.permission.mysql.PermissionMySQL;
import de.moritz.core.system.player.mysql.PlayerMySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author: toLowerCase
 */
public class MySQLUtils {

    private static final String host = "localhost";
    private static final String username = "Moritz";
    private static final String password = "moritz3001";
    private static final String database = "spigotcore";
    private static final int port = 3306;

    public static Connection getConnection( ) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection( "jdbc:mysql://" + host + ":" + port + "/" + database +
                    "?autoReconnect=true", username, password );
            System.out.println( "[MySQL] Verbindung mit MySQL aufgebaut!" );
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        }
        return connection;
    }

    private static PermissionMySQL permissionMySQL;
    private static PlayerMySQL playerMySQL;

    public static void load( ) {
        MySQLUtils.permissionMySQL = new PermissionMySQL();
        MySQLUtils.permissionMySQL.connect();
        MySQLUtils.playerMySQL = new PlayerMySQL();
        MySQLUtils.playerMySQL.connect();
    }

    public static void disconnect( ) {
        MySQLUtils.permissionMySQL.disconnect();
        MySQLUtils.playerMySQL.disconnect();
    }

    public static PermissionMySQL getPermissionMySQL( ) {
        return MySQLUtils.permissionMySQL;
    }

    public static PlayerMySQL getPlayerMySQL( ) {
        return MySQLUtils.playerMySQL;
    }
}
