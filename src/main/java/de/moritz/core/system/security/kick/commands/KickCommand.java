package de.moritz.core.system.security.kick.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import de.moritz.core.system.server.utils.Methods;

/**
 * @author: toLowerCase
 */
public class KickCommand extends Command {

    public KickCommand( ) {
        super( "kick", "core.command.kick", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( strings[0] );
            if ( target == null ) {
                player.sendMessage( "§cKick", "§cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            kick( player, target, "Verhalten" );
            return;
        }
        if ( strings.length > 1 ) {
            GrafikPlayer target = GrafikPlayer.getPlayer( strings[0] );
            if ( target == null ) {
                player.sendMessage( "§cKick", "§cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            StringBuilder reason = new StringBuilder();
            for ( int i = 1; i < strings.length; i++ ) {
                reason.append( strings[i] ).append( " " );
            }
            kick( player, target, reason.toString() );
            return;
        }
        player.sendMessage( "§cKick", "§cNutze§8: §c/kick §7[§fSpieler§7] §7<§fReason...§7>" );
    }

    private void kick( GrafikPlayer player, GrafikPlayer target, String reason ) {
        Methods.sendTeamler( "§cKick", "§7Der Spieler " + target.getDisplayName() + " §7wurde von "
                + player.getDisplayName() + " §7gekickt." );
        Methods.sendTeamler( "§cKick", "§7Grund §8➟ §e" + reason );
        target.kick( reason );
    }
}
