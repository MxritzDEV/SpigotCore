package de.moritz.core.system.server.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

/**
 * @author: toLowerCase
 */
public class WhereamiCommand extends Command {

    public WhereamiCommand( ) {
        super( "whereami", null, false, "wobinich" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Du befindest dich auf §e" +
                player.getServer().getName() + "§7." );
    }
}
