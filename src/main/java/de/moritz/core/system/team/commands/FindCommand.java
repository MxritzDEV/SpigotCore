package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class FindCommand extends Command {

    public FindCommand( ) {
        super( "find", "core.command.find", true, new String[0] );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer( strings[0] );
            if ( target == null ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der Spieler " + target.getDisplayName() +
                    " §7befindet sich auf §e" + target.getServer().getInfo().getName() + "§7." );
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/find §7[§fSpieler§7]" );
    }

    @Override
    public void onConsole( CommandSender commandSender, String[] strings ) {
        if ( strings.length == 1 ) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer( strings[0] );
            if ( target == null ) {
                commandSender.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDer Spieler §e" + strings[0] +
                        " §cist aktuell nicht online!" );
                return;
            }
            commandSender.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Der Spieler " + target.getDisplayName() +
                    " §7befindet sich auf §e" + target.getServer().getInfo().getName() + "§7." );
            return;
        }
        commandSender.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze: /find §7[§fSpieler§7]" );
    }
}
