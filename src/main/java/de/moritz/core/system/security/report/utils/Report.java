package de.moritz.core.system.security.report.utils;

import com.google.common.collect.Maps;
import de.moritz.core.system.player.GrafikPlayer;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.HashMap;

/**
 * @author: toLowerCase
 */
public class Report {

    private static HashMap<GrafikPlayer, Report> reporthashMap = Maps.newHashMap();
    private GrafikPlayer reporter;
    private GrafikPlayer player;

    public Report( GrafikPlayer reporter, GrafikPlayer player ) {
        this.reporter = reporter;
        this.player = reporter;
        this.reporter.sendMessage( "§cReport", "§cBist du sicher " + player.getDisplayName() + " §cfür §eHACKING §czu reporten?" );
        TextComponent textComponent = new TextComponent( "§7" );
        this.reporter.sendMessage( "§cReport", textComponent );
    }

    public void confirm( ) {

    }

    public void cancel( ) {

    }

    public void accept( GrafikPlayer teamler ) {

    }

    public void deny( GrafikPlayer teamler ) {

    }

    public GrafikPlayer getPlayer( ) {
        return this.player;
    }

    public GrafikPlayer getReporter( ) {
        return this.reporter;
    }

    public static HashMap<GrafikPlayer, Report> getReports( ) {
        return reporthashMap;
    }
}
