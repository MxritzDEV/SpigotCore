package de.moritz.core.system.security.kick.listeners;

import de.moritz.core.system.player.GrafikPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author: toLowerCase
 */
public class KickListener implements Listener {

    @EventHandler
    public void onKick( ServerKickEvent event ) {
        GrafikPlayer player = GrafikPlayer.getPlayer( event.getPlayer() );
        event.setCancelled( true );
        event.setCancelServer( ProxyServer.getInstance().getServerInfo( "Lobby-1" ) );
        player.sendMessage( "§cSystem", "§cDie Verbindung zu §e" + event.getKickedFrom().getName() +
                " §cwurde geschlossen." );
        player.sendMessage( "§cSystem", "§cGrund §8➟ §e" + event.getKickReason() );
    }
}
