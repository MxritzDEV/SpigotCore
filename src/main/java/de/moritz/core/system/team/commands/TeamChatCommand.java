package de.moritz.core.system.team.commands;

import de.moritz.core.system.permission.utils.GroupType;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import de.moritz.core.system.server.utils.Methods;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class TeamChatCommand extends Command {

    public TeamChatCommand( ) {
        super( "teamchat", "core.command.teamchat", false, "tc" );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 0 ) {
            player.sendMessage( "§bTeam", "§cNutze§8: §c/tc §7[§fNachricht...§7] §cfür den Teamchat!" );
            player.sendMessage( "§bTeam", "§7Hier siehst du alle §eTeammitglieder§7, die zurzeit Online sind." );
            for ( Integer id : PermGroup.getGroups().keySet() ) {
                PermGroup permGroup = PermGroup.getGroup( id );
                if ( permGroup.getGroupType().equals( GroupType.USER ) )
                    continue;
                player.sendMessage( "§bTeam", permGroup.getColor() + permGroup.getName() );
                for ( UUID uuid : PermUser.getPermUsers().keySet() ) {
                    PermUser permUser = PermUser.getUser( uuid );
                    if ( permUser.getGroup().getId() != permGroup.getId() )
                        continue;
                    ProxiedPlayer teamler = ProxyServer.getInstance().getPlayer( permUser.getUUID() );
                    if ( teamler == null )
                        continue;
                    player.sendMessage( "    §8- §7[" + teamler.getServer().getInfo().getName() + "] " +
                            teamler.getDisplayName() );
                }
            }
            return;
        }
        if ( !player.isNotify() ) {
            player.sendMessage( "§bTeam", "§cDu hast die §eNachrichten §causgeschaltet." +
                    " Um in den TeamChat zu schreiben benutze bitte /notify." );
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append( "§7[" ).append( player.getServer().getName() ).append( "] " )
                .append( player.getDisplayName() ).append( " §8➟ §6" );

        for ( String string : strings ) {
            builder.append( string ).append( " " );
        }

        Methods.sendTeamler( "§bTeam", builder.toString().replace( "&", "§" ) );
    }
}
