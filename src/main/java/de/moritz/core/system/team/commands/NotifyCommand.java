package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;

/**
 * @author: toLowerCase
 */
public class NotifyCommand extends Command {

    public NotifyCommand( ) {
        super( "notify", "core.command.notfiy", false );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( player.isNotify() ) {
            player.sendMessage( "§bTeam", "§7Du erhältst jetzt keine §cNachrichten §7mehr. " );
            player.setNotify( !player.isNotify() );
            return;
        }
        player.sendMessage( "§bTeam", "§7Du erhältst jetzt wieder §aNachrichten§7. " );
        player.setNotify( !player.isNotify() );
    }
}
