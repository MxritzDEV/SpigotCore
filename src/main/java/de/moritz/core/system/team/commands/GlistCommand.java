package de.moritz.core.system.team.commands;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author: toLowerCase
 */
public class GlistCommand extends Command {

    public GlistCommand( ) {
        super( "glist", "core.command.glist", true, new String[0] );
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( strings.length == 1 ) {
            if ( strings[0].equalsIgnoreCase( "all" ) ) {
                for ( String key : ProxyServer.getInstance().getServers().keySet() ) {
                    sendList( player, ProxyServer.getInstance().getServerInfo( key ) );
                }
                return;
            }
            try {
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( strings[0] );
                sendList( player, serverInfo );
            } catch ( NullPointerException ex ) {
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cDieser Server ist nicht verfügbar." );
                player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Hier eine Liste aller verfügbaren Server." );
                for ( String key : ProxyServer.getInstance().getServers().keySet() ) {
                    player.sendMessage( "    §8- §e" + ProxyServer.getInstance().getServerInfo( key ).getName() );
                }
            }
            return;
        }
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §cNutze§8: §c/glist §7[§fServer/all§7]" );
    }

    private void sendList( GrafikPlayer player, ServerInfo serverInfo ) {
        player.sendMessage( "§8┃ §c● §8┃ §cSystem §8× §7Servername §8➟ §e" + serverInfo.getName() +
                " §8- §7Spíeler §8➟ §e" + serverInfo.getPlayers().size() );
        for ( ProxiedPlayer all : serverInfo.getPlayers() ) {
            player.sendMessage( "    §8- " + all.getDisplayName() );
        }
    }
}
