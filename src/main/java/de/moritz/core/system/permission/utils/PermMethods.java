package de.moritz.core.system.permission.utils;

import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.server.utils.Methods;
import de.moritz.core.system.utils.UUIDFetcher;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermMethods {

    public static void createUser( GrafikPlayer player, UUID uuid, PermGroup permGroup, int days ) {
        if ( uuid == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        if ( PermUser.getUser( uuid ) != null ) {
            player.sendMessage( "§6Permissions", "§cDiesen User gibt es schon!" );
            player.sendMessage( "§6Permissions", "§cUm einen User zu editieren, benutze bitte" +
                    " /perm user edit..." );
            return;
        }
        PermUser permUser = new PermUser( uuid );
        permUser.setName( UUIDFetcher.getName( uuid ) );
        permUser.setGroup( permGroup );
        permUser.setExpiration( days );
        permUser.create();
        Methods.send( "§6Permissions", "§7Der User " + permGroup.getColor() +
                permUser.getName() + " §7wurde erstellt.", "core.command.permission" );
        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
            try {
                byteOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream( byteOutputStream );
                dataOutputStream.writeUTF( "createUser" );
                dataOutputStream.writeUTF( permUser.getUUID().toString() );
                dataOutputStream.writeUTF( permUser.getGroup().getId() + "" );
                serverInfo.sendData( "Permission", byteOutputStream.toByteArray() );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                try {
                    dataOutputStream.close();
                    byteOutputStream.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void createGroup( GrafikPlayer player, int id, String name, String shortName, String color,
                                    boolean useFullName, GroupType groupType, boolean isDefault ) {
        if ( PermGroup.getGroup( id ) != null ) {
            player.sendMessage( "§6Permissions", "§cDie ID ist schon vergeben!" );
            return;
        }
        PermGroup permGroup = new PermGroup( id );
        permGroup.setName( name );
        permGroup.setShortName( shortName );
        permGroup.setColor( color );
        permGroup.setUseFullName( useFullName );
        permGroup.setGroupType( groupType );
        permGroup.setDefaultGroup( isDefault );
        permGroup.create();

        Methods.send( "§6Permissions", "§7Die Gruppe " + permGroup.getColor() +
                permGroup.getName() + " §7wurde erstellt.", "core.command.permission" );
    }

    public static void removeUser( GrafikPlayer player, UUID uuid ) {
        if ( uuid == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        PermUser permUser = PermUser.getUser( uuid );
        if ( permUser == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        if ( ProxyServer.getInstance().getPlayer( permUser.getUUID() ) != null ) {
            GrafikPlayer.getPlayer( permUser.getUUID() ).quit(
                    "§8┃ §6● §8┃ §6GrafikbugDE §8× §aRangänderung\n\n" +
                            "§7Du hast ein neuen §eRang §7erhalten.\n" +
                            "§7Neuer Rang §8➟ " + PermGroup.getDefault().getColor() + PermGroup.getDefault().getName() );
        }

        Methods.send( "§6Permissions", "§7Der User " + permUser.getGroup().getColor() +
                        permUser.getName() + " §7wurde gelöscht.",
                "core.command.permission" );
        permUser.remove();
    }

    public static void removeGroup( GrafikPlayer player, PermGroup permGroup ) {
        if ( permGroup == null ) {
            player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
            return;
        }
        Methods.send( "§6Permissions", "§7Die Gruppe " + permGroup.getColor() +
                        permGroup.getName() + " §7wurde gelöscht.",
                "core.command.permission" );
        permGroup.delete();
    }

    public static void infoUser( GrafikPlayer player, UUID uuid ) {
        if ( uuid == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        PermUser permUser = PermUser.getUser( uuid );
        if ( permUser == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        player.sendMessage( "§6Permissions", "§7Name §8➟ " + permUser.getGroup().getColor() +
                permUser.getName() );
        player.sendMessage( "§6Permissions", "§7UUID §8➟ §e" + permUser.getUUID() );
        player.sendMessage( "§6Permissions", "§7Rang §8➟ " + permUser.getGroup().getColor() +
                permUser.getGroup().getName() );
        Date date = new Date( permUser.getExpiration() );
        SimpleDateFormat dateFormat = new SimpleDateFormat( "dd.MM.YYYY - hh:mm:ss" );
        long time = ( permUser.getExpiration() - System.currentTimeMillis() ) / 1000L;
        int days = (int) time / 24 / 60 / 60;

        player.sendMessage( "§6Permissions", "§7Verbleibende Zeit §8➟ §e" + ( permUser.getExpiration()
                == -1 ? "§cPermanent" : "§e" + days + " §7Tage" ) );

        player.sendMessage( "§6Permissions", "§7Ende §8➟ §e" + ( permUser.getExpiration() == -1 ?
                "§cNie" : dateFormat.format( date ) + " Uhr" ) );
    }

    public static void infoGroup( GrafikPlayer player, PermGroup permGroup ) {
        if ( permGroup == null ) {
            player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
            return;
        }
        player.sendMessage( "§6Permissions", "§7ID §8➟ §e" + permGroup.getId() );
        player.sendMessage( "§6Permissions", "§7Name §8➟ " + permGroup.getColor() + permGroup.getName() );
        player.sendMessage( "§6Permissions", "§7ShortName §8➟ §e" + permGroup.getColor() +
                permGroup.getShortName() );
        player.sendMessage( "§6Permissions", "§7Color §8➟ " + permGroup.getColor() + permGroup.getColor()
                .replace( "§", "&" ) );
        player.sendMessage( "§6Permissions", "§7UseFullName §8➟ " + ( permGroup.isUseFullName()
                ? "§atrue" : "§cfalse" ) );
        player.sendMessage( "§6Permissions", "§7GroupType §8➟ §e" + permGroup.getGroupType().name() );
        player.sendMessage( "§6Permissions", "§7IsDefaultGroup §8➟ " + ( permGroup.isDefaultGroup()
                ? "§atrue" : "§cfalse" ) );
        if ( permGroup.getPermissions().isEmpty() )
            return;
        player.sendMessage( "§6Permissions", "§7Permissions §8➟" );
        for ( String permission : permGroup.getPermissions() ) {
            player.sendMessage( "    §8- §e" + permission );
        }
    }

    public static void editUser( GrafikPlayer player, UUID uuid, String what, String update ) {
        if ( uuid == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        PermUser permUser = PermUser.getUser( uuid );
        if ( permUser == null ) {
            player.sendMessage( "§6Permissions", "§cDieser User ist nicht verfügbar!" );
            return;
        }
        if ( what.equalsIgnoreCase( "group" ) ) {
            PermGroup permGroup = null;
            try {
                permGroup = PermGroup.getGroup( Integer.parseInt( update ) );
            } catch ( NumberFormatException ex ) {
                player.sendMessage( "§6Permissions", "§cNutze§8: §c/perm user edit §7[§fName§7] " +
                        "§cgroup §7[§fID§7]" );
                return;
            }
            if ( permGroup == null ) {
                player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
                return;
            }
            permUser.setGroup( permGroup );
            Methods.send( "§6Permissions", "§7Die Gruppe von " + permGroup.getColor()
                    + permUser.getName() + " §7wurde zu " + permGroup.getColor() + permGroup.getName() +
                    " §7geändert.", "core.command.permission" );
            if ( ProxyServer.getInstance().getPlayer( permUser.getUUID() ) != null ) {
                GrafikPlayer.getPlayer( permUser.getUUID() ).quit(
                        "§8┃ §6● §8┃ §6GRAFIKBUG§7.§6de §8× §aRangänderung\n\n" +
                                "§7Du hast ein neuen §eRang §7erhalten.\n" +
                                "§7Neuer Rang §8➟ " + permGroup.getColor() + permGroup.getName() );
            }

            ByteArrayOutputStream byteOutputStream = null;
            DataOutputStream dataOutputStream = null;
            for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
                try {
                    byteOutputStream = new ByteArrayOutputStream();
                    dataOutputStream = new DataOutputStream( byteOutputStream );
                    dataOutputStream.writeUTF( "updateGroup" );
                    dataOutputStream.writeUTF( permUser.getUUID().toString() );
                    dataOutputStream.writeUTF( permUser.getGroup().getId() + "" );
                    serverInfo.sendData( "Permission", byteOutputStream.toByteArray() );
                } catch ( Exception e ) {
                    e.printStackTrace();
                } finally {
                    try {
                        dataOutputStream.close();
                        byteOutputStream.close();
                    } catch ( IOException e ) {
                        e.printStackTrace();
                    }
                }
            }
            return;
        }
        if ( what.equalsIgnoreCase( "time" ) ) {
            int days = -1;
            try {
                days = Integer.parseInt( update );
            } catch ( NumberFormatException ex ) {
                player.sendMessage( "§6Permissions", "§cNutze§8: §c/perm user edit §7[§fName§7] " +
                        "§ctime §7[§fTage§7]" );
                return;
            }
            permUser.setExpiration( days );
            Methods.send( "§6Permissions", "§7Die Verbleibene von " + permUser.getGroup().getColor()
                    + permUser.getName() + " §7wurde auf " + ( days == -1 ? "§cPermanent" : "§e" + days + " Tage" ) +
                    " §7geändert.", "core.command.permission" );
            reload();
            return;
        }
        player.sendMessage( "§6Permissions", "§cDu kannst nur die Gruppe §7(§fgroup§7) §coder die " +
                "Verbleibene Zeit §7(§ftime§7) §cändern." );
    }

    public static void editGroup( GrafikPlayer player, PermGroup permGroup ) {
        if ( permGroup == null ) {
            player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
            return;
        }
    }

    public static void addPermission( GrafikPlayer player, PermGroup permGroup, String permission ) {
        if ( permGroup == null ) {
            player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
            return;
        }
        if ( permGroup.hasPermission( permission ) ) {
            player.sendMessage( "§6Permissions", "§cDie Gruppe " +
                    permGroup.getColor() + permGroup.getName() + " §chat bereits die Permission §e"
                    + permission + "§c." );
            return;
        }
        permGroup.addPermission( permission );

        Methods.send( "§6Permissions", "§7Der Gruppe "
                + permGroup.getColor() + permGroup.getName() + " §7wurde die Permission §e" + permission
                + " §7hinzugefügt.", "core.command.permission" );
        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
            try {
                byteOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream( byteOutputStream );
                dataOutputStream.writeUTF( "addPermission" );
                dataOutputStream.writeUTF( permGroup.getId() + "" );
                dataOutputStream.writeUTF( permission );
                serverInfo.sendData( "Permission", byteOutputStream.toByteArray() );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                try {
                    dataOutputStream.close();
                    byteOutputStream.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void removePermission( GrafikPlayer player, PermGroup permGroup, String permission ) {
        if ( permGroup == null ) {
            player.sendMessage( "§6Permissions", "§cDiese Gruppe ist nicht verfügbar!" );
            return;
        }
        if ( !permGroup.hasPermission( permission ) ) {
            player.sendMessage( "§6Permissions", "§cDie Gruppe " + permGroup.getColor()
                    + permGroup.getName() + " §chat die Permission §e" + permission + " §cnoch nicht." );
            return;
        }
        permGroup.removePermission( permission );

        Methods.send( "§6Permissions", "§7Der Gruppe "
                + permGroup.getColor() + permGroup.getName() + " §7wurde die Permission §e" + permission
                + " §7entfernt.", "core.command.permission" );
        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
            try {
                byteOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream( byteOutputStream );
                dataOutputStream.writeUTF( "removePermission" );
                dataOutputStream.writeUTF( permGroup.getId() + "" );
                dataOutputStream.writeUTF( permission );
                serverInfo.sendData( "Permission", byteOutputStream.toByteArray() );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                try {
                    dataOutputStream.close();
                    byteOutputStream.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void reload( ) {
        PermManager.save();

        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        for ( String server : ProxyServer.getInstance().getServers().keySet() ) {
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo( server );
            if ( !serverInfo.getName().toLowerCase().contains( "lobby" ) )
                continue;
            try {
                byteOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream( byteOutputStream );
                dataOutputStream.writeUTF( "reload" );
                serverInfo.sendData( "Permission", byteOutputStream.toByteArray() );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                try {
                    dataOutputStream.close();
                    byteOutputStream.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void sendHelp( GrafikPlayer player ) {
        player.sendMessage( "§6Permissions", "§cPermissionsSystem §8- §cHilfe" );
        player.sendMessage( "§6Permissions", "§e/perm user info §7[§fName§7]" );
        player.sendMessage( "§6Permissions", "§e/perm user create §7[§fName§7] §7[§fID§7] [§fTage§7]" );
        player.sendMessage( "§6Permissions", "§e/perm user edit §7[§fName§7]" );
        player.sendMessage( "§6Permissions", "§e/perm user delete §7[§fName§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group info §7[§fID§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group create §7[§fID§7] §7[§fName§7] §7[§fShortName§7] §7[§fColor§7] " +
                "§7[§fUseFullName§7] §7[§fGroupType§7] §7[§fIsDefault§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group edit §7[§fID§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group delete §7[§fID§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group §7[§fID§7] §eadd §7[§fPermission§7]" );
        player.sendMessage( "§6Permissions", "§e/perm group §7[§fID§7] §eremove §7[§fPermission§7]" );
    }
}
