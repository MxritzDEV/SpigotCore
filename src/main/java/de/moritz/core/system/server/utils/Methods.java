package de.moritz.core.system.server.utils;

import de.moritz.core.system.player.GrafikPlayer;

/**
 * @author: toLowerCase
 */
public class Methods {

    public static void sendTeamler( String prefix, String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( "core.team" ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( prefix, message );
        }
    }

    public static void sendTeamler( String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( "core.team" ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( message );
        }
    }

    public static void sendAdmins( String prefix, String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( "core.team.admin" ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( prefix, message );
        }
    }

    public static void sendAdmins( String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( "core.team.admin" ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( message );
        }
    }

    public static void send( String prefix, String message, String permission ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( permission ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( prefix, message );
        }
    }

    public static void send( String message, String permission ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            if ( !all.hasPermission( permission ) )
                continue;
            if ( !all.isNotify() )
                continue;
            all.sendMessage( message );
        }
    }

    public static void sendAll( String prefix, String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            all.sendMessage( prefix, message );
        }
    }

    public static void sendAll( String message ) {
        for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
            all.sendMessage( message );
        }
    }
}
