package de.moritz.core.system.player;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class GrafikPlayer implements PlayerInfo {

    private static HashMap<UUID, GrafikPlayer> players = Maps.newHashMap();
    private static Set<GrafikPlayer> onlinePlayers = Sets.newHashSet();
    private ProxiedPlayer player;
    private GrafikOfflinePlayer offlinePlayer;
    private UUID uuid;
    private String name;
    private String ip;
    private boolean notify;
    private int bans;
    private int mutes;
    private int kicks;
    private long firstJoin;
    private long lastLeave;
    private long onlineTime;

    public GrafikPlayer( ProxiedPlayer player ) {
        this.player = player;
    }

    public void login( ) {
        this.offlinePlayer = GrafikOfflinePlayer.getOfflinePlayer( player.getUniqueId() );
        this.uuid = player.getUniqueId();
        this.name = player.getName();
        this.ip = player.getAddress().getAddress().toString().replace( "/", "" );
        this.notify = true;
        this.bans = this.offlinePlayer.getBans();
        this.mutes = this.offlinePlayer.getMutes();
        this.kicks = this.offlinePlayer.getKicks();
        this.firstJoin = this.offlinePlayer.getFirstJoin();
        this.lastLeave = this.offlinePlayer.getLastLeave();
        this.onlineTime = this.offlinePlayer.getOnlineTime();

        GrafikPlayer.players.put( player.getUniqueId(), this );
        GrafikPlayer.onlinePlayers.add( this );
    }

    public void logut( ) {
        this.offlinePlayer.setName( this.name );
        this.offlinePlayer.setIp( this.ip );
        this.offlinePlayer.setBans( this.bans );
        this.offlinePlayer.setMutes( this.mutes );
        this.offlinePlayer.setKicks( this.kicks );
        this.offlinePlayer.setLastLeave( System.currentTimeMillis() );
        this.offlinePlayer.setOnlineTime( this.loadOnlineTime() );

        GrafikPlayer.players.remove( this.uuid );
        GrafikPlayer.onlinePlayers.remove( this );
    }


    public void setNotify( boolean notify ) {
        this.notify = notify;
    }

    public void setBans( int bans ) {
        this.bans = bans;
    }

    public void setMutes( int mutes ) {
        this.mutes = mutes;
    }

    public void setKicks( int kicks ) {
        this.kicks = kicks;
    }

    public void setFirstJoin( long firstJoin ) {
        this.firstJoin = firstJoin;
    }

    public void setLastLeave( long lastLeave ) {
        lastLeave = lastLeave;
    }

    public void setOnlineTime( long onlineTime ) {
        this.onlineTime = onlineTime;
    }

    public ProxiedPlayer getPlayer( ) {
        return this.player;
    }

    public GrafikOfflinePlayer getOfflinePlayer( ) {
        return this.offlinePlayer;
    }

    @Override
    public UUID getUUID( ) {
        return this.uuid;
    }

    @Override
    public String getName( ) {
        return this.name;
    }

    @Override
    public String getIP( ) {
        return this.ip;
    }

    public boolean isNotify( ) {
        return this.notify;
    }

    @Override
    public int getBans( ) {
        return this.bans;
    }

    @Override
    public int getMutes( ) {
        return this.mutes;
    }

    @Override
    public int getKicks( ) {
        return this.kicks;
    }

    @Override
    public long getFirstJoin( ) {
        return this.firstJoin;
    }

    @Override
    public long getLastLeave( ) {
        return this.lastLeave;
    }

    private long loadOnlineTime( ) {
        long time = System.currentTimeMillis();
        time -= this.lastLeave;
        return time += this.onlineTime;
    }

    @Override
    public long getOnlineTime( ) {
        return this.onlineTime;
    }

    @Override
    public int getOnlineHours( ) {
        return (int) ( this.onlineTime / 3600000 );
    }

    public static GrafikPlayer getPlayer( ProxiedPlayer player ) {
        return GrafikPlayer.players.get( player.getUniqueId() );
    }

    public static GrafikPlayer getPlayer( String name ) {
        return GrafikPlayer.getPlayer( ProxyServer.getInstance().getPlayer( name ) );
    }

    public static GrafikPlayer getPlayer( UUID uuid ) {
        return GrafikPlayer.players.get( uuid );
    }

    public static Set<GrafikPlayer> getOnlinePlayers( ) {
        return GrafikPlayer.onlinePlayers;
    }

    public boolean equals( GrafikPlayer grafikPlayer ) {
        return grafikPlayer.getName().equals( this.name ) &&
                grafikPlayer.getUUID().toString().equals( this.uuid.toString() );
    }

    //  PLAYER METHODS

    public void sendMessage( String prefix, String message ) {
        String colorCode = "c";
        if ( prefix.startsWith( "§" ) ) {
            colorCode = prefix.substring( 1, 2 );
        }
        this.player.sendMessage( "§8§l┃ §" + colorCode + "● §8┃ " + prefix + " §8× §7" + message );
    }

    public void sendMessage( String message ) {
        this.player.sendMessage( message );
    }

    public void sendMessage( String prefix, TextComponent component ) {
        TextComponent prefixComonent = new TextComponent( prefix );
        this.player.sendMessage( prefixComonent, component );
    }

    public void sendMessage( TextComponent... component ) {
        this.player.sendMessage( component );
    }

    public void sendActionbar( String message ) {
        this.player.sendMessage( ChatMessageType.ACTION_BAR, new TextComponent( message ) );
    }

    public void sendTitle( String title, String subTitle ) {
        sendTitle( title, subTitle, 9, 17, 3 );
    }

    public void sendTitle( String title, String subTitle, int fadeIn, int stay, int fadeOut ) {
        Title bungeeTitle = ProxyServer.getInstance().createTitle();
        bungeeTitle.title( new TextComponent( title ) );
        bungeeTitle.subTitle( new TextComponent( subTitle ) );
        bungeeTitle.fadeIn( fadeIn );
        bungeeTitle.stay( stay );
        bungeeTitle.fadeOut( fadeIn );
        this.player.sendTitle( bungeeTitle );
    }

    public PermGroup getPermGroup( ) {
        return PermUser.getUser( this.uuid ).getGroup();
    }

    public String getDisplayName( ) {
        return PermUser.getUser( this.uuid ).getGroup().getColor() + this.name;
    }

    public boolean hasPermission( String permission ) {
        return this.player.hasPermission( permission );
    }

    public void teleport( ServerInfo serverInfo ) {
        if ( this.player.getServer().getInfo().equals( serverInfo ) )
            return;
        this.player.connect( serverInfo );
    }

    public void quit( String message ) {
        this.player.disconnect( message );
    }

    public void kick( String reason ) {
        this.kicks = kicks + 1;
        quit( "§8§l┃ §6● §8┃ §6GrafikbugDE §8× §aKick\n\n" +
                "§7Du wurdest vom Server gekickt.\n" +
                "§7Grund §8➟ §e" + reason );
    }

    public void teleport( GrafikPlayer player ) {
        teleport( player.getServer() );
        ServerInfo serverInfo = player.getServer();

        ByteArrayOutputStream byteOutputStream = null;
        DataOutputStream dataOutputStream = null;
        try {
            byteOutputStream = new ByteArrayOutputStream();
            dataOutputStream = new DataOutputStream( byteOutputStream );
            dataOutputStream.writeUTF( "teleport" );
            dataOutputStream.writeUTF( this.uuid.toString() );
            dataOutputStream.writeUTF( player.getUUID().toString() );
            serverInfo.sendData( "Player", byteOutputStream.toByteArray() );
        } catch ( Exception e ) {
            e.printStackTrace();
        } finally {
            try {
                dataOutputStream.close();
                byteOutputStream.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }

    public ServerInfo getServer( ) {
        return this.player.getServer().getInfo();
    }

    public void setTabHeader( String header, String footer ) {
        this.player.setTabHeader( new TextComponent( header ), new TextComponent( footer ) );
    }

    public int getPing( ) {
        return this.player.getPing();
    }
}
