package de.moritz.core.system.permission.utils;

import de.moritz.core.system.mysql.MySQLUtils;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class PermManager {

    public static void init( ) {
        PermGroup.setGroups( MySQLUtils.getPermissionMySQL().getGroups() );
        PermUser.setPermUsers( MySQLUtils.getPermissionMySQL().getUsers() );
    }

    public static void save( ) {
        for ( Integer id : PermGroup.getGroups().keySet() ) {
            PermGroup permGroup = PermGroup.getGroup( id );
            permGroup.save();
        }

        for ( UUID uuid : PermUser.getPermUsers().keySet() ) {
            PermUser permUser = PermUser.getUser( uuid );
            permUser.save();
        }
    }
}
