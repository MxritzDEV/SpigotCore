package de.moritz.core.system.modus.listeners;

import de.moritz.core.Main;
import de.moritz.core.system.modus.utils.ProxyState;
import de.moritz.core.system.permission.utils.PermUser;
import de.moritz.core.system.player.GrafikOfflinePlayer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

/**
 * @author: toLowerCase
 */
public class ModusListener implements Listener {

    @EventHandler
    public void onProxyPing( ProxyPingEvent event ) {
        ServerPing serverPing = event.getResponse();

        if ( Main.getProxyState().getDisplayName() != null ) {
            ServerPing.Protocol protocol = serverPing.getVersion();
            protocol.setProtocol( 1 );
            protocol.setName( Main.getProxyState().getDisplayName() );
            serverPing.setVersion( protocol );
        }

        event.setResponse( serverPing );
    }

    @EventHandler
    public void onLogin( LoginEvent event ) {
        UUID uuid = event.getConnection().getUniqueId();
        ProxyState state = Main.getProxyState();
        if ( GrafikOfflinePlayer.hasAccount( uuid ) ) {
            GrafikOfflinePlayer offlinePlayer = GrafikOfflinePlayer.getOfflinePlayer( uuid );
            if ( state.getPermission() == null )
                return;
            if ( state.getKickreason() == null )
                return;
            if ( offlinePlayer.hasPermission( state.getPermission() ) )
                return;
            event.setCancelled( true );
            event.setCancelReason( state.getKickreason() );
            return;
        }
        PermUser user = PermUser.getUser( uuid );
        if ( state.getPermission() == null )
            return;
        if ( state.getKickreason() == null )
            return;
        if ( user.hasPermission( state.getPermission() ) || user.hasPermission( "*" ) )
            return;
        event.setCancelled( true );
        event.setCancelReason( state.getKickreason() );
    }
}
