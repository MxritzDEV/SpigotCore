package de.moritz.core.system.team.commands;

import de.moritz.core.Main;
import de.moritz.core.system.modus.utils.ProxyState;
import de.moritz.core.system.permission.utils.PermManager;
import de.moritz.core.system.player.GrafikPlayer;
import de.moritz.core.system.player.utils.PlayerManager;
import de.moritz.core.system.server.utils.Command;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;

import java.util.concurrent.TimeUnit;

/**
 * @author: toLowerCase
 */
public class EndCommand extends Command {

    private static boolean end = false;
    private int countdown;
    private int zahl;

    public EndCommand( ) {
        super( "end", "core.command.end", true );
        this.countdown = 65;
        this.zahl = 60;
    }

    @Override
    public void onPlayer( GrafikPlayer player, String[] strings ) {
        if ( end ) {
            for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                all.sendMessage( "" );
                all.sendMessage( "§cSystem", "§cDer Netzwerkneustart wurde abgebrochen!" );
                all.sendMessage( "" );
            }
            ProxyServer.getInstance().getScheduler().cancel( Main.getInstance() );
            end = false;
            this.countdown = 65;
            this.zahl = 60;
            return;
        }
        if ( strings.length == 1 ) {
            if ( strings[0].equalsIgnoreCase( "now" ) ) {
                this.countdown = 15;
                this.zahl = 10;
                startCount();
                return;
            }
            player.sendMessage( "§cSystem", "§cNutze§8: §c/end §7<§fnow§7>" );
            return;
        }
        startCount();
    }

    @Override
    public void onConsole( CommandSender commandSender, String[] strings ) {
        if ( end ) {
            for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                all.sendMessage( "" );
                all.sendMessage( "§cSystem", "§cDer Netzwerkneustart wurde abgebrochen!" );
                all.sendMessage( "" );
            }
            ProxyServer.getInstance().getScheduler().cancel( Main.getInstance() );
            end = false;
            this.countdown = 65;
            this.zahl = 60;
            return;
        }
        if ( strings.length == 1 ) {
            if ( strings[0].equalsIgnoreCase( "now" ) ) {
                this.countdown = 15;
                this.zahl = 10;
                startCount();
                return;
            }
            commandSender.sendMessage( "§8§l┃ §c● §8┃ §cSystem §8× §cNutze: /end §7<§fnow§7>" );
            return;
        }
        startCount();
    }

    private void startCount( ) {
        end = true;
        ProxyServer.getInstance().getScheduler().schedule( Main.getInstance(), ( ) -> {
            if ( zahl == 60 || zahl == 50 || zahl == 40 || zahl == 30 ||
                    zahl == 20 || zahl == 10 || zahl < 6 && zahl > 1 ) {
                for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                    all.sendMessage( "§cSystem", "§cDer Server wird in §e" + zahl +
                            " §cSekunden neugestartet." );
                }
            }
            if ( zahl == 1 ) {
                for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                    all.sendMessage( "§cSystem", "§cDer Server wird in §e" + zahl +
                            " §cSekunde neugestartet." );
                }
            }
            if ( zahl < 6 && zahl > 0 ) {
                for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                    all.sendTitle( "§c" + zahl, "" );
                }
            }
            if ( countdown == 5 ) {
                Main.setProxyState( ProxyState.STOPPING );
                for ( GrafikPlayer all : GrafikPlayer.getOnlinePlayers() ) {
                    all.quit( Main.getProxyState().getKickreason() );
                }
            }
            if ( countdown == 3 ) {
                PermManager.save();
                PlayerManager.save();
                Main.getReportManager().stop();
            }
            if ( countdown == 0 ) {
                ProxyServer.getInstance().getScheduler().cancel( Main.getInstance() );
                ProxyServer.getInstance().stop();
                return;
            }
            countdown--;
            zahl--;
        }, 0, 1, TimeUnit.SECONDS );
    }
}