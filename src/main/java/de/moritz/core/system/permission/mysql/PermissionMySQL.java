package de.moritz.core.system.permission.mysql;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import de.moritz.core.system.mysql.Database;
import de.moritz.core.system.mysql.MySQLUtils;
import de.moritz.core.system.permission.utils.GroupType;
import de.moritz.core.system.permission.utils.PermGroup;
import de.moritz.core.system.permission.utils.PermUser;
import de.moritz.core.system.utils.UUIDFetcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * @author: toLowerCase
 */
public class PermissionMySQL implements Database<Connection> {

    private Connection connection;
    private ExecutorService pool;

    @Override
    public void connect( ) {
        this.connection = MySQLUtils.getConnection();
        this.pool = Executors.newCachedThreadPool();
        createTable();
    }

    @Override
    public void disconnect( ) {
        try {
            this.connection.close();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        this.pool.shutdown();
    }

    @Override
    public void createTable( ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS perm__group " +
                        "(GROUPID VARCHAR (100), GROUPNAME VARCHAR (100), SHORTNAME VARCHAR (100), COLOR VARCHAR (100)," +
                        " USEFULLNAME VARCHAR (100), TYPE VARCHAR (100), ISDEFAULT VARCHAR (100))" );
                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS perm__users " +
                        "(UUID VARCHAR (100), NAME VARCHAR (100), GROUPID VARCHAR (100), TIME BIGINT (100))" );
                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS perm__permissons " +
                        "(GROUPID VARCHAR (100), PERMISSION VARCHAR (100))" );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    /*
     *  PERMISSION - GROUP
     */

    public void addGroup( PermGroup permGroup ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "INSERT INTO perm__group " +
                        "(GROUPID, GROUPNAME, SHORTNAME, COLOR, USEFULLNAME, TYPE, ISDEFAULT) VALUES " +
                        "(?, ?, ?, ?, ?, ?, ?)" );
                preparedStatement.setInt( 1, permGroup.getId() );
                preparedStatement.setString( 2, permGroup.getName() );
                preparedStatement.setString( 3, permGroup.getShortName() );
                preparedStatement.setString( 4, permGroup.getColor().replace( "§", "&" ) );
                preparedStatement.setString( 5, permGroup.isUseFullName() + "" );
                preparedStatement.setString( 6, permGroup.getGroupType().name() );
                preparedStatement.setString( 7, permGroup.isDefaultGroup() + "" );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public void removeGroup( PermGroup permGroup ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "DELETE FROM perm__group WHERE GROUPID = ?" );
                preparedStatement.setInt( 1, permGroup.getId() );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public void updateGroup( PermGroup permGroup ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET GROUPNAME = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.getName() );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET SHORTNAME = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.getShortName() );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET COLOR = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.getColor() );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET USEFULLNAME = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.isUseFullName() + "" );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET TYPE = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.getGroupType().name() );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__group SET ISDEFAULT = ? " +
                        "WHERE GROUPID = ?" );
                preparedStatement.setString( 1, permGroup.isDefaultGroup() + "" );
                preparedStatement.setInt( 2, permGroup.getId() );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        clearPermission( permGroup );
    }

    public boolean hasPermissions( PermGroup permGroup ) {
        boolean hasPermissions = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                hasPermissions = true;
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        return hasPermissions;
    }

    private void fillPermission( PermGroup permGroup ) {
        if ( permGroup.getPermissions().isEmpty() )
            return;
        PreparedStatement preparedStatement = null;
        for ( String permission : permGroup.getPermissions() ) {
            try {
                preparedStatement = this.connection.prepareStatement( "INSERT INTO perm__permissons " +
                        "(GROUPID, PERMISSION) VALUES (?, ?)" );
                preparedStatement.setInt( 1, permGroup.getId() );
                preparedStatement.setString( 2, permission );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void clearPermission( PermGroup permGroup ) {
        if ( !hasPermissions( permGroup ) ) {
            fillPermission( permGroup );
            return;
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = this.connection.prepareStatement( "DELETE FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            preparedStatement.executeUpdate();
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        fillPermission( permGroup );
    }


    public Set<String> getPermissions( PermGroup permGroup ) {
        Set<String> permissions = Sets.newHashSet();
        if ( !hasPermissions( permGroup ) )
            return permissions;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__permissons " +
                    "WHERE GROUPID = ?" );
            preparedStatement.setInt( 1, permGroup.getId() );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                permissions.add( resultSet.getString( "PERMISSION" ) );
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch ( SQLException e ) {
                e.printStackTrace();
            }
        }
        return permissions;
    }

    public HashMap<Integer, PermGroup> getGroups( ) {
        HashMap<Integer, PermGroup> groups = Maps.newHashMap();
        try {
            this.pool.submit( ( ) -> {
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                try {
                    preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__group" );
                    resultSet = preparedStatement.executeQuery();
                    while ( resultSet.next() ) {
                        int id = resultSet.getInt( "GROUPID" );
                        PermGroup permGroup = new PermGroup( id );
                        permGroup.setName( resultSet.getString( "GROUPNAME" ) );
                        permGroup.setShortName( resultSet.getString( "SHORTNAME" ) );
                        permGroup.setColor( resultSet.getString( "COLOR" ) );
                        permGroup.setUseFullName( resultSet.getString( "USEFULLNAME" ).equals( "true" ) );
                        try {
                            permGroup.setGroupType( GroupType.valueOf( resultSet.getString( "TYPE" ).toUpperCase() ) );
                        } catch ( IllegalArgumentException ex ) {
                            permGroup.setGroupType( GroupType.USER );
                        }
                        permGroup.setPermissions( getPermissions( permGroup ) );
                        permGroup.setDefaultGroup( resultSet.getString( "ISDEFAULT" ).equals( "true" ) );
                        groups.put( id, permGroup );
                    }
                } catch ( SQLException ex ) {
                    ex.printStackTrace();
                } finally {
                    try {
                        resultSet.close();
                        preparedStatement.close();
                    } catch ( SQLException e ) {
                        e.printStackTrace();
                    }
                }
            } ).get( 2, TimeUnit.SECONDS );
        } catch ( InterruptedException | TimeoutException | ExecutionException e ) {
            e.printStackTrace();
        }
        return groups;
    }

    /*
     *  PERMISSION - USER
     */

    public boolean existsUser( UUID uuid ) {
        final boolean[] exists = { false };
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT UUID FROM perm__users WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString() );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    exists[0] = true;
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return exists[0];
    }

    public void addUser( UUID uuid, PermGroup permGroup, long time ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "INSERT INTO perm__users " +
                        "(UUID, NAME, GROUPID, TIME) VALUES (?, ?, ?, ?)" );
                preparedStatement.setString( 1, uuid.toString() );
                preparedStatement.setString( 2, UUIDFetcher.getName( uuid ) );
                preparedStatement.setInt( 3, permGroup.getId() );
                preparedStatement.setLong( 4, time );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public void removeUser( UUID uuid ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "DELETE FROM perm__users WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString() );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public void updateUser( PermUser permUser ) {
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = this.connection.prepareStatement( "UPDATE perm__users SET GROUPID = ?" +
                        " WHERE UUID = ?" );
                preparedStatement.setInt( 1, permUser.getGroup().getId() );
                preparedStatement.setString( 2, permUser.getUUID().toString() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__users SET NAME = ?" +
                        " WHERE UUID = ?" );
                preparedStatement.setString( 1, permUser.getName() );
                preparedStatement.setString( 2, permUser.getUUID().toString() );
                preparedStatement.executeUpdate();

                preparedStatement = this.connection.prepareStatement( "UPDATE perm__users SET TIME = ?" +
                        " WHERE UUID = ?" );
                preparedStatement.setLong( 1, permUser.getExpiration() );
                preparedStatement.setString( 2, permUser.getUUID().toString() );
                preparedStatement.executeUpdate();
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public HashMap<UUID, PermUser> getUsers( ) {
        HashMap<UUID, PermUser> users = Maps.newHashMap();
        this.pool.execute( ( ) -> {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = this.connection.prepareStatement( "SELECT * FROM perm__users" );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                    UUID uuid = UUID.fromString( resultSet.getString( "UUID" ) );
                    PermUser permUser = new PermUser( uuid );
                    permUser.setName( resultSet.getString( "NAME" ) );
                    permUser.setGroup( PermGroup.getGroups().get( resultSet.getInt( "GROUPID" ) ) );
                    permUser.setExpiration( resultSet.getLong( "TIME" ) );
                    users.put( uuid, permUser );
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace();
            } finally {
                try {
                    resultSet.close();
                    preparedStatement.close();
                } catch ( SQLException e ) {
                    e.printStackTrace();
                }
            }
        } );
        return users;
    }
    /*
     *  UTILS
     */

    @Override
    public boolean isConnected( ) {
        return this.connection != null;
    }

    @Override
    public Connection getConnection( ) {
        return this.connection;
    }
}
